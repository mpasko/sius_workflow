
function readSingleFile(evt,callback) {
	//Retrieve the first (and only!) File from the FileList object
    var f = evt.target.files[0]; 

	if (f) {
		var r = new FileReader();
		r.onload = function(e) { 
			var xml = e.target.result;
			loadFromFile(xml, callback, function(){
				//On error
                console.log("xml content: %s",xml);
				alert("Request failed");
			});
		}
		r.readAsText(f);
		
		return f.name;
	} else { 
		alert("Failed to load file");
	}
}

function saveSingleFile(data_json,filename) {
    saveToFile(data_json, function(data_xml){
        var textFileAsBlob = new Blob([data_xml], {
            type: 'text/plain'
        });
        var downloadLink = document.createElement("a");
        downloadLink.download = filename;
        downloadLink.innerHTML = "Download File";
        downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
        downloadLink.onclick = function(event) {
            document.body.removeChild(event.target);
        };
        downloadLink.style.display = "none";
        document.body.appendChild(downloadLink);
        downloadLink.click();
        console.log(output);
    }, function () {
        console.log("Request failed for: %s",JSON.stringify(data_json));
    });
}


/*
   Function: saveToFile
   Używa backendu do konwersji formatów (z JSON na XML).
	*/
var saveToFile = function(query,callback,onerror) {
	var ready = JSON.stringify(query);
	var resource = 'http://localhost:8080/jboss-html5/rest/conversion/save';
	var ourType = 'application/json';
	jQuery.ajax({
		type: 'POST',
		contentType: ourType,
		dataType: 'text',
//		crossDomain: true,
		url: resource,
		data: query,
//		headers: {'Access-Control-Allow-Origin:': '*'},
		cache: false,
		processData: false,
		success: function(data) {
			callback(data);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("%s %s %s",XMLHttpRequest, textStatus, errorThrown);
			onerror();
		}
	});
}


/*
   Function: loadFromFile
   Używa backendu do konwersji formatów (z XML na JSON).
	*/
var loadFromFile = function(query,callback,onerror) {
	var ready = escape(query);
	var resource = 'http://localhost:8080/jboss-html5/rest/conversion/load';
	var ourType = 'text/plain';
	jQuery.ajax({
		type: 'GET',
		contentType: ourType,
		dataType: 'json',
//		crossDomain: true,
		url: resource,
		data: ready,
//		headers: {'Access-Control-Allow-Origin:': '*'},
		cache: false,
		success: function(data) {
			callback(data);
		},
		processData: false,
		error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log("%s %s %s",XMLHttpRequest, textStatus, errorThrown);
			onerror();
		}
	});
}