var graph = new joint.dia.Graph;
var propertyMap = {};

var current_filename = "Untitled.bpmn";



var linkNodes = function(source, target, label, vertices) {
    
    var cell = new joint.shapes.fsa.Arrow({
        source: { id: source },
        target: { id: target },
        labels: [{ position: .5, attrs: { text: { text: label || '', 'font-weight': 'bold' } } }],
        vertices: vertices || []
    });
    graph.addCell(cell);
    return cell;
}


var paper = new joint.dia.Paper({		//arkusz diagramu
    el: $('#paper-container'),
    width: 2000,
    height: 2000,
    model: graph,
    gridSize: 10,

}).on({

    'blank:pointerdown': function(evt,x,y) {

    },

    'cell:pointerdown': function(cellView, evt) {

    },

    'cell:pointerup': function(cellView, evt, x, y) {
// Find the first element below that is not a link nor the dragged element itself.
        var elementBelow = graph.get('cells').find(function(cell) {
            if (cell instanceof joint.dia.Link) return false; // Not interested in links.
            if (cell instanceof joint.shapes.fsa.EndState) return false;
            if (cell.id === cellView.model.id) return false; // The same element as the dropped one.
            if (cell.getBBox().containsPoint(g.point(x, y))) {
                return true;
            }
            return false;
        });
        if ((cellView.model instanceof joint.dia.Link)|| 
        (cellView.model instanceof joint.shapes.fsa.StartState)) {
            return [];
        }
        // If the two elements are connected already, don't
        // connect them again (this is application specific though).
        if (elementBelow && !_.contains(graph.getNeighbors(elementBelow), cellView.model)) {
            
            linkNodes(elementBelow.id, cellView.model.id, 'Next/True', []);
            // Move the element a bit to the side.
            cellView.model.translate(200, 0);
        }
    }
});

var manual = new joint.shapes.basic.Rect({
    position: { x: 100, y: 30 },
    size: { width: 100, height: 30 },
    attrs: { rect: { 'stroke-width': '5', 'stroke-opacity': .7, stroke: 'black', rx: 3, ry: 3, fill: 'lightblue' }, text: { text: 'Manual action', 'font-size': 10, style: { 'text-shadow': '1px 1px 1px lightgray' } } }
});

var automatic = new joint.shapes.basic.Rect({
    position: { x: 100, y: 30 },
    size: { width: 100, height: 30 },
    attrs: { rect: { 'stroke-width': '5', 'stroke-opacity': .7, stroke: 'black', rx: 3, ry: 3, fill: 'lightgreen' }, text: { text: 'Automatic action', 'font-size': 10, style: { 'text-shadow': '1px 1px 1px lightgray' } } }
});
var decision = new joint.shapes.basic.Rect({
    position: { x: 100, y: 30 },
    size: { width: 50, height: 50 },
    angle: 45,
    attrs: { rect: { 'stroke-width': '5', 'stroke-opacity': .7, stroke: 'black', rx: 3, ry: 3, fill: 'white' }, text: { text: 'Decision', 'font-size': 10, transform:'rotate(-45 40 10)', style: { 'text-shadow': '1px 1px 1px lightgray' } } }
});
var start = new joint.shapes.fsa.StartState({ position: { x: 100, y: 30 } });
var stop = new joint.shapes.fsa.EndState({ position: { x: 100, y: 30 } });

var link = new joint.dia.Link({
    source: { id: manual.id },
    target: { id: automatic.id }
});


/*
   Function: recognize
   Rozpoznaje bloczek.
	*/

function recognize(cell){
    if ((cell.attributes.type == "basic.Rect")&&(cell.attributes.attrs.rect.fill=='lightgreen')){
        return 'automatic';
    }
    if ((cell.attributes.type == "basic.Rect")&&(cell.attributes.attrs.rect.fill=='lightblue')){
        return 'manual';
    }
    if ((cell.attributes.type == "basic.Rect")&&(cell.attributes.attrs.rect.fill=='white')){
        return 'decision';
    }
    if (cell.attributes.type == "fsa.StartState") {
        return 'start';
    }
    if (cell.attributes.type == "fsa.EndState") {
        return 'stop';
    }
    if (cell.attributes.type == "fsa.Arrow") {
        return 'flow';
    }
}

jQuery("#btn-manual").bind('click', function(){
    var rect = manual.clone();
    graph.addCells([rect]);
});

jQuery("#btn-automatic").bind('click', function(){
    var rect = automatic.clone();
    graph.addCells([rect]);
});

jQuery("#btn-start").bind('click', function(){
    var circ = start.clone();
    graph.addCells([circ]);
});

jQuery("#btn-stop").bind('click', function(){
    var circ = stop.clone();
    graph.addCells([circ]);
});

jQuery("#btn-decision").bind('click', function(){
    var dec = decision.clone();
    graph.addCells([dec]);
});

jQuery("#btn-flow").bind('click', function(){
//   var nw_link = new joint.dia.Link();
//   nw_link.attr({
//        '.marker-source': { d: 'M 10 0 L 0 5 L 10 10 z'}
//    });
//    nw_link.set('vertices', [{x: 100, y: 30},{x: 100, y: 100}]);
//    graph.addCells([nw_link]);
});

jQuery("#btn-save").bind('click', function(){
    var graph_json = graph.toJSON();
    var all_json = {layout:graph_json,properties:propertyMap};
    var data_json = JSON.stringify(all_json);
    saveSingleFile(data_json,current_filename);
    console.log("%s",data_json);
});

jQuery("#btn-clear").bind('click', function(){
    graph.clear();
    current_filename = "Untitled.bpmn";
});

/*
   Function: openFile
   Otwiera plik.
	*/

var openFile = function(evt){
    var fn = readSingleFile(evt, function(data) {
        console.log("data received: %s", data);
        graph.fromJSON(data.layout);
        propertyMap=data.properties;
    });
    current_filename = fn;
};

document.getElementById('fileinput')
	.addEventListener('change', openFile, false);
    