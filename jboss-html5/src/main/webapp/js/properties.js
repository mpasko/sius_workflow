/*
   Function: getDefaultProperty
   Inicjalizuje domyślną właściwość.
	*/

function getDefaultProperty(type,id){
    switch(type){
        case 'automatic':
            
            return {
                'id':id,
                'name':"Automatic action"
            };
        case 'manual':
            
            return {
                'id':id,
                'name':"Manual Action"
            };
        case 'decision':
            
            return {
                'id':id,
                'name':"Manual Action"
            };
            
        default:
            return {
                'id':id,
                'name':'Next/True'
            };
    }
}

var router = new ViewRouter(jQuery('#statusbar-container'));
Backbone.history.start();

function updateModelAutomatic(model){
    var id = model.get("id");
    if (propertyMap[cell.id] == "undefined") {
        propertyMap[cell.id] = getDefaultProperty(type,id);
    }
}

var AutomaticModel = Backbone.Model.extend({
    defaults:getDefaultProperty('automatic',0),
    initializee: function(){
        this.on("change",updateModelAutomatic);
    }
});

/*
   Function: updateModelManual
   Przypisuje aktualny model do widoku.
	*/

function updateModelManual(model){
    var id = model.get("id");
    if (propertyMap[cell.id] == "undefined") {
        propertyMap[cell.id] = getDefaultProperty(type,id);
    }
}

var ManualModel = Backbone.Model.extend({
    defaults:getDefaultProperty('automatic',0),
    initializee: function(){
        this.on("change",updateModelAutomatic);
    }
});

graph.on('change', function(cell){
    if (typeof(cell)!=="undefined"){
        var type = recognize(cell);    
        console.log('item on_change:'+cell.id);
        if (propertyMap[cell.id] == "undefined") {
            propertyMap[cell.id] = getDefaultProperty(type);
        }
        switch(type){
            case 'automatic':
                var model = new AutomaticModel(propertyMap[cell.id]);
                router.automaticView.setModel(model);
                router.automatic();
                break;
                
            case 'manual':
                var model = new ManualModel(propertyMap[cell.id]);
                router.manualView.setModel(model);
                router.manual();
                break;
                
            case 'decision':
                var model = new ManualModel(propertyMap[cell.id]);
                router.decisionView.setModel(model);
                router.decision();
                break;
        }
    }
});