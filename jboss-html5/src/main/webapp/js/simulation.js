var started=false;

/*
   Function: clearActive
   Wyczyszczenie aktywnych bloczkow.
	*/

function clearActive() {
    var array = graph.getElements();
    for (var item=0; item<array.length; item++){
        var cell = array[item];
        if (typeof(cell)!=="undefined"){
            if (cell.attributes.type == "basic.Rect"){
                cell.attr({text:{fill:'black'}});
                cell.attr({rect:{stroke:'black'}});
            }
            if ((cell.attributes.type == "fsa.StartState") || (cell.attributes.type == "fsa.EndState")) {
                cell.attr({circle: {fill:'black'}});
            }
        }
    }
}  

/*
   Function: processActive
   Wczytuje z backendu ktore bloczki sa aktywne
	*/
 
function processActive(active) {
    clearActive();
    if (active.length == 0){
        stop_simulation();
    }
    for (var id=0; id<active.length; id++){
        var cell = graph.getCell(active[id]);
        if (typeof(cell)!=="undefined"){
            if (cell.attributes.type == "basic.Rect"){
                cell.attr({text:{fill:'green'}});
                cell.attr({rect:{stroke:'green'}});
            }
            if ((cell.attributes.type == "fsa.StartState") || (cell.attributes.type == "fsa.EndState")) {
                cell.attr({circle: {fill:'green'}});
            }
        }
    }
}


/*
   Function: long_polling
   Pętla Reverse Ajax.
	*/
  
function long_polling() { 
    jQuery.getJSON('simulation', function(active) { 
        if (started) {
            processActive(active);
            long_polling();
        }
    }); 
}

function stop_simulation() {
    jQuery('.status').empty().append("Simulation stopped");
    jQuery("#sim-stop").unbind('click');
    jQuery("#sim-start").bind('click', start_simulation);
    started = false;
    clearActive();
}

function start_simulation() {
    jQuery('.status').empty().append("Simulation running");
    jQuery("#sim-start").unbind('click');
    jQuery("#sim-stop").bind('click', stop_simulation);
    started = true;
    var json = graph.toJSON();
    var message = JSON.stringify(json);
    jQuery.ajax({
		type: 'POST',
		contentType: 'application/json',
		dataType: 'text',
		url: 'simulation',
		data: message,
		cache: false,
		processData: false,
		success: function(data) {
			long_polling();
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
            stop_simulation();
            alert("Simulation startup failed!");
		}
	});
}
  
jQuery("#sim-start").bind('click', start_simulation);