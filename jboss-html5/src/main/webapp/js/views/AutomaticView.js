var AutomaticView = ContentView.extend({


	/*
   Function: initialize
   Inicjalizuje za pomocą template-id.
	*/
	initialize: function(options) {
		this.template = options.template;
	},

	/*
   Function: render
   Pobiera zawartość template i wstawia do nowego elementu.
	*/
	
	render: function() {
		var content = jQuery(this.template).html();
		jQuery(this.el).html(content);

		return this;
	}

});