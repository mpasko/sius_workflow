var ContentView = Backbone.View.extend({

	/*
   Function: initialize
   Inicjalizuje za pomocą template-id.
	*/
	initialize: function(options) {
		this.template = options.template;
	},

	/*
   Function: render
   Pobiera zawartość template i wstawia do nowego elementu.
	*/
	render: function() {
		var content = jQuery(this.template).html();
		jQuery(this.el).html(content);

		return this;
	},

	
	
	/*
   Function: setModel
   Ustawianie modelu.
	*/
	
    setModel: function(m){
        this.model = m;
        this.listenTo(this.model, 'change', this.render);
        this.listenTo(this.model, 'destroy', this.remove);
    },
    
	/*
   Function: remove
   Usuwanie elementu.
	*/
    remove: function() {
		$(this.el).empty().detach();

		return this;
	}
});