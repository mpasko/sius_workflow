var ViewRouter = Backbone.Router.extend({


/*
   Function: initialize
   Inicjalizacja.
	*/

	initialize: function(el) {
		this.el = el;

		this.automaticView = new AutomaticView({template: '#automatic-template'});
		this.manualView = new ManualView({template: '#manual-template'});
		this.decisionView = new ManualView({template: '#decision-template'});
		this.notFoundView = new ContentView({template: '#not-found'});
	},

	routes: {
		"": "notFound",
		"automatic": "automatic",
		"manual": "manual",
		"decision": "decision",
		"*else": "notFound"
	},

	currentView: null,

	
	
	
	/*
   Function: clearInOut
   Usuwa liste parametrów wejściowych do procesu.
	*/
    clearInOut: function(){
        jQuery('#input').empty().append('<b>Input parameters</b>').append('</br>');
        jQuery('#output').empty().append('<b>Output parameters</b>').append('</br>');
    },
    
	
	
	/*
   Function: setAddInFunction
   Ustawia nowy parametr wejściowy do procesu.
	*/
    setAddInFunction: function() {
        jQuery('#btn-add-in').bind('click', function(){
            var name = jQuery('#in-name').val();
            jQuery('#input').append(name).append('</br>');
        });
    },
    
	
	/*
   Function: setAddOutFunction
   Ustawia nowy parametr wyjściowy do procesu.
	*/
    setAddOutFunction: function() {
        jQuery('#btn-add-out').bind('click', function(){
            var name = jQuery('#out-name').val();
            jQuery('#output').append(name).append('</br>');
        });
    },
    
	
	/*
   Function: switchView
   Przełącza widok.
	*/
	switchView: function(view) {
		if (this.currentView) {
			// Detach the old view
			this.currentView.remove();
		}

		// Move the view element into the DOM (replacing the old content)
		this.el.html(view.el);

		// Render view after it is in the DOM (styles are applied)
		view.render();
        this.clearInOut();
        this.setAddInFunction();
        this.setAddOutFunction();
		this.currentView = view;
	},

	
	
	setActiveEntry: function(url) {
		
	},

	/*
   Function: automatic
   Ustawianie właściwości bloczka do automatycznego.
	*/

	automatic: function() {
		this.switchView(this.automaticView);
		this.setActiveEntry('#automatic');
	},

	
	/*
   Function: manual
   Ustawianie właściwości bloczka do manualnego.
	*/
	manual: function() {
		this.switchView(this.manualView);
		this.setActiveEntry('#manual');
	},
    
	
	/*
   Function: decision
   Ustawanie właściwości bloczka do decyzyjnego.
	*/
    decision: function() {
		this.switchView(this.decisionView);
		this.setActiveEntry('#decision');
	},

	
	/*
   Function: notFound
   Obsługa błędnego widoku.
	*/
	notFound: function() {
		this.switchView(this.notFoundView);
	}

});