package org.sius.workflow.view;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * POJO który zawiera atrybuty etykiety przy strzałce
 * @author marcin
 *
 */
@XmlRootElement
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class LabelsDescriptor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 52936354064032826L;
	private Double position;
	private Attrs attrs;

	public LabelsDescriptor() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Zwraca atrybuty
	 * @return atrybuty
	 */
	public Attrs getAttrs() {
		return attrs;
	}
	
	/**
	 * Pobiera atrybuty
	 * @param attrs atrybuty
	 */
	public void setAttrs(Attrs attrs) {
		this.attrs = attrs;
	}

	/**
	 * Współrzędne elementu
	 * @return Współrzędne elementu
	 */
	public Double getPosition() {
		return position;
	}

	/**
	 * Współrzędne elementu
	 * @param position Współrzędne elementu
	 */
	public void setPosition(Double position) {
		this.position = position;
	}

	/**
	 * Fabryka domyślnego deskryptora etykiety dla strzałki
	 * @return Domyślny deskryptor
	 */
	public static LabelsDescriptor ArrowLabelFactory() {
		LabelsDescriptor label = new LabelsDescriptor();
		label.position = 0.5;
		label.attrs = Attrs.ArrowAttrsFactory();
		return label;
	}

}
