package org.sius.workflow.view;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Jest to węzeł, który agreguje widok diagramu
 * @author marcin
 *
 */
@XmlRootElement
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Graph implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4059386976091613673L;
	
	private List<Cell> cells;

	public Graph(){
		this.cells = new LinkedList<Cell>();
	}

	/**
	 * Zwraca listę komórek
	 * @return Lista elementów
	 */
	@JsonProperty
	public List<Cell> getCells() {
		return cells;
	}

	/**
	 * Pobiera listę komórek
	 * @param cells Lista elementów
	 */
	public void setCells(List<Cell> cells) {
		this.cells = cells;
	}

	/**
	 * Dodaje komórkę
	 * @param cell Nowa komórka
	 */
	public void addCell(Cell cell) {
		cells.add(cell);
	}

}
