package org.sius.workflow.view;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Zawiera wszystkie atrybuty każdego rodzaju komórki
 * @author marcin
 *
 */
@XmlRootElement
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Cell implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8072291884890197824L;
	private IdDescriptor id = new IdDescriptor();
	private String type;
	private Size size=null;
	private Position position=null;
	private Integer angle=0;
	private String embeds="";
	private Integer z=0;
	private Attrs attrs=null;
	private static Integer maxZ=0;
	// Below only valid for Arrow type
	private Boolean smooth=null;
	private IdDescriptor source=null;
	private IdDescriptor target=null;
	private List<LabelsDescriptor> labels=null;
	private List<Object> vertices=null;
	
	/**
	 * Fabryka komórki starowej
	 * @param x Pozycja x
	 * @param y Pozycja y
	 * @return Komórka startowa
	 */
	public static Cell StartStateFactory(int x, int y){
		Cell cell = new Cell();
		cell.type = "fsa.StartState";
		cell.size = new Size(20,20);
		cell.position = new Position(x, y);
		cell.attrs = new Attrs();
		return cell;
	}
	
	/**
	 * Fabryka komórki końcowej
	 * @param x Pozycja x
	 * @param y Pozycja y
	 * @return Komórka końcowa
	 */
	public static Cell StopStateFactory(int x, int y){
		Cell cell = new Cell();
		cell.type = "fsa.EndState";
		cell.size = new Size(20,20);
		cell.position = new Position(x, y);
		cell.attrs = new Attrs();
		return cell;
	}
	
	/**
	 * Fabryka komórki akcji
	 * @param x Pozycja x
	 * @param y Pozycja y
	 * @param type Typ komórki (MANUAL lub AUTOMATIC)
	 * @return Komórka akcji
	 */
	public static Cell ActionFactory(int x, int y, Type type){
		Cell cell = new Cell();
		cell.type = "basic.Rect";
		cell.size = new Size(100,30);
		cell.position = new Position(x, y);
		cell.attrs = Attrs.ActionFactory(type);
		return cell;
	}
	
	/**
	 * Fabryka komórki decyzyjnej
	 * @param x Pozycja x
	 * @param y Pozycja y
	 * @return Komórka decyzyjna
	 */
	public static Cell DecisionFactory(int x, int y){
		Cell cell = new Cell();
		cell.type = "basic.Rect";
		cell.angle = 45;
		cell.size = new Size(50,50);
		cell.position = new Position(x, y);
		cell.attrs = Attrs.ActionFactory(Type.DECISION);
		return cell;
	}
	
	/**
	 * Fabryka strzałki
	 * @param from Źródło
	 * @param to Cel
	 * @return Element strzałki
	 */
	public static Cell ArrowFactory(Cell from, Cell to){
		Cell cell = new Cell();
		cell.type = "fsa.Arrow";
		cell.smooth = true;
		cell.source = new IdDescriptor(from.getId());
		cell.target = new IdDescriptor(to.getId());
		cell.labels = new LinkedList<LabelsDescriptor>();
		cell.labels.add(LabelsDescriptor.ArrowLabelFactory());
		cell.vertices = new LinkedList<Object>();
		cell.attrs = new Attrs();
		return cell;
	}
	
	/**
	 * Rozpoznaje rodzaj komórki
	 * @return rodzaj komórki
	 */
	public Type recognize(){
		if (this.type.equalsIgnoreCase("fsa.StartState")){
			return Type.START;
		}
		if (this.type.equalsIgnoreCase("fsa.EndState")){
			return Type.STOP;
		}
		if (this.type.equalsIgnoreCase("fsa.Arrow")){
			return Type.ARROW;
		}
		if (this.type.equalsIgnoreCase("basic.Rect")){
			TextDescriptor txt = this.attrs.getText();
			if(txt.getText().equalsIgnoreCase("Manual action")){
				return Type.MANUAL;
			}
			if(txt.getText().equalsIgnoreCase("Manual action")){
				return Type.AUTOMATIC;
			}
			if(txt.getText().equalsIgnoreCase("Decision")){
				return Type.DECISION;
			}
		}
		return null;
	}
	
	public Cell(){
		this.z = maxZ++;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Size getSize() {
		return size;
	}
	public void setSize(Size size) {
		this.size = size;
	}
	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}
	public Integer getAngle() {
		return angle;
	}
	public void setAngle(Integer angle) {
		this.angle = angle;
	}
	public String getEmbeds() {
		return embeds;
	}
	public void setEmbeds(String embeds) {
		this.embeds = embeds;
	}
	public Integer getZ() {
		return z;
	}
	public void setZ(Integer z) {
		this.z = z;
	}

	public Attrs getAttrs() {
		return attrs;
	}

	public void setAttrs(Attrs attrs) {
		this.attrs = attrs;
	}

	public Boolean getSmooth() {
		return smooth;
	}

	public void setSmooth(Boolean smooth) {
		this.smooth = smooth;
	}

	public IdDescriptor getSource() {
		return source;
	}

	public void setSource(IdDescriptor source) {
		this.source = source;
	}

	public IdDescriptor getTarget() {
		return target;
	}

	public void setTarget(IdDescriptor target) {
		this.target = target;
	}

	public List<LabelsDescriptor> getLabels() {
		return labels;
	}

	public void setLabels(List<LabelsDescriptor> labels) {
		this.labels = labels;
	}

	public List<Object> getVertices() {
		return vertices;
	}

	public void setVertices(List<Object> vertices) {
		this.vertices = vertices;
	}

	/**
	 * Jest to unikalny identyfikator elementu GUID
	 * @return ID
	 */
	public String getId() {
		return id.getId();
	}

	/**
	 * Jest to unikalny identyfikator elementu GUID
	 * @param id ID
	 */
	public void setId(String id) {
		this.id.setId(id);
	}
}
