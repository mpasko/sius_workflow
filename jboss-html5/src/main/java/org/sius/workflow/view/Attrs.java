package org.sius.workflow.view;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * POJO na wizualne atrybuty elementu diagramu
 * @author marcin
 *
 */
@XmlRootElement
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Attrs implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6788976364776448806L;
	private TextDescriptor text=null;
	private RectDescriptor rect=null;

	public Attrs() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Zwraca deskryptor zawartego napisu
	 * @return Deskryptor napisu
	 */
	public TextDescriptor getText() {
		return text;
	}

	/**
	 * Pobiera deskryptor zawartego napisu
	 * @param text Deskryptor napisu
	 */
	public void setText(TextDescriptor text) {
		this.text = text;
	}

	/**
	 * Zwraca właściwości prostokąta składającego się na element
	 * @return Właściwości prostokąta
	 */
	public RectDescriptor getRect() {
		return rect;
	}

	/**
	 * Pobiera właściwości prostokąta składającego się na element
	 * @param rect Właściwości prostokąta
	 */
	public void setRect(RectDescriptor rect) {
		this.rect = rect;
	}

	/**
	 * Fabryka domyślnych ustawień dla strzałki
	 * @return Atrybuty
	 */
	public static Attrs ArrowAttrsFactory() {
		Attrs attrs = new Attrs();
		attrs.text = new TextDescriptor();
		attrs.text.setFont_weight("bold");
		attrs.text.setText("Next/True");
		return attrs;
	}

	/**
	 * Fabryka domyślnych ustawień dla elementu
	 * @param type typ elementu
	 * @return Atrybuty
	 */
	public static Attrs ActionFactory(Type type) {
		Attrs attrs = new Attrs();
		attrs.rect = new RectDescriptor();
		attrs.rect.setStroke_width("5");
		attrs.rect.setStroke_opacity(0.7);
		attrs.rect.setRx(3);
		attrs.rect.setRy(3);
		attrs.text = new TextDescriptor();
		attrs.text.setStyle(StyleDescriptor.ActionStyleFactory(type));
		attrs.text.setFont_size(10);
		if(type==Type.MANUAL){
			attrs.text.setText("Manual action");
			attrs.rect.setFill("lightblue");
		}else if(type==Type.AUTOMATIC){
			attrs.text.setText("Automatic action");
			attrs.rect.setFill("lightgreen");
		}else{
			attrs.text.setText("Decision");
			attrs.text.setTransform("rotate(-45 40 10)");
			attrs.rect.setFill("white");
		}
		return attrs;
	}

}
