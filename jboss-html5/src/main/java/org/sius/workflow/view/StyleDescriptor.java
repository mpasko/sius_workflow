package org.sius.workflow.view;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Styl tekstu opisany w formacie stringa
 * @author marcin
 *
 */
@XmlRootElement
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class StyleDescriptor implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6532196886834395938L;
	private String text_shadow;
	
	public StyleDescriptor() {
		// TODO Auto-generated constructor stub
	}

	@XmlAttribute(name="text-shadow")
	@JsonProperty("text-shadow")
	public String getText_shadow() {
		return text_shadow;
	}

	@JsonProperty("text-shadow")
	public void setText_shadow(String text_shadow) {
		this.text_shadow = text_shadow;
	}

	/**
	 * Generuje domyślny styl dla elementu akcji
	 * @param type Typ elementy (MANUAL lub AUTOMATIC)
	 * @return Deskryptor stylu
	 */
	public static StyleDescriptor ActionStyleFactory(Type type) {
		StyleDescriptor style = new StyleDescriptor();
		style.text_shadow="1px 1px 1px lightgray";
		return style;
	}

}
