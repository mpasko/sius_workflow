package org.sius.workflow.view;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Szerokosc i wysokośc elementu w pikselach
 * @author marcin
 *
 */
@XmlRootElement
public class Size {
	private Integer width=10;
	private Integer height=10;
	
	public Size(int i, int j) {
		width = i;
		height = j;
	}
	
	public Size(){
		
	}
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	public Integer getWidth() {
		return width;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
}
