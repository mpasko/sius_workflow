package org.sius.workflow.view;

import java.io.Serializable;
import java.util.UUID;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Jest to POJO który zawiera jedynie GUID
 * @author marcin
 *
 */
@XmlRootElement
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class IdDescriptor implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7060568910995718359L;
	protected String id;

	public IdDescriptor() {
		this.id = UUID.randomUUID().toString();
	}
	
	/**
	 * Inicjalizuje samego siebie wartością id
	 * @param id GUID
	 */
	public IdDescriptor(String id) {
		this.id = id;
	}

	/**
	 * Zwraca GUID
	 * @return GUID
	 */
	public String getId() {
		return id;
	}

	/**
	 * Pobiera GUID
	 * @param id GUID
	 */
	public void setId(String id) {
		this.id = id;
	}

}
