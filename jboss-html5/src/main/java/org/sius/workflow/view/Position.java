package org.sius.workflow.view;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Współrzędne X i Y elementu na diagramie
 * @author marcin
 *
 */
@XmlRootElement
public class Position {
	private Integer x=100;
	private Integer y=100;
	
	public Position(int x2, int y2) {
		x = x2;
		y = y2;
	}
	
	public Position() {
	}
	public Integer getX() {
		return x;
	}
	public void setX(Integer x) {
		this.x = x;
	}
	public Integer getY() {
		return y;
	}
	public void setY(Integer y) {
		this.y = y;
	}
}
