package org.sius.workflow.view;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Opisuje style prostokąta należącego do elementu diagramu
 * @author marcin
 *
 */
@XmlRootElement
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RectDescriptor implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1097277855575686597L;
	private String fill;
	private String stroke;
	private String stroke_width;
	private Double stroke_opacity;
	private Integer rx;
	private Integer ry;

	public RectDescriptor() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Kolor wypełnienia
	 * @return wypełnienie
	 */
	public String getFill() {
		return fill;
	}

	/**
	 * Kolor wypełnienia
	 * @param fill wypełnienie
	 */
	public void setFill(String fill) {
		this.fill = fill;
	}

	@XmlAttribute(name="stroke-width")
	@JsonProperty("stroke-width")
	public String getStroke_width() {
		return stroke_width;
	}

	@JsonProperty("stroke-width")
	public void setStroke_width(String stroke_width) {
		this.stroke_width = stroke_width;
	}

	@XmlAttribute(name="stroke-opacity")
	@JsonProperty("stroke-opacity")
	public Double getStroke_opacity() {
		return stroke_opacity;
	}

	@JsonProperty("stroke-opacity")
	public void setStroke_opacity(Double d) {
		this.stroke_opacity = d;
	}

	public Integer getRx() {
		return rx;
	}

	public void setRx(Integer rx) {
		this.rx = rx;
	}

	public Integer getRy() {
		return ry;
	}

	public void setRy(Integer ry) {
		this.ry = ry;
	}

	public String getStroke() {
		return stroke;
	}

	public void setStroke(String stroke) {
		this.stroke = stroke;
	}

}
