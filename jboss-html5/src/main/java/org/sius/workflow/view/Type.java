package org.sius.workflow.view;

/**
 * Określa typ elementu diagramu
 * @author marcin
 *
 */
public enum Type {
	START, STOP, ARROW, AUTOMATIC, MANUAL, DECISION
}
