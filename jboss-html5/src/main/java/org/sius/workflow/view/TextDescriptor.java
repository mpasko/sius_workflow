package org.sius.workflow.view;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@XmlRootElement
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TextDescriptor implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2963847328898344345L;
	private String text;
	private String font_weight=null;
	private Integer font_size=null;
	private StyleDescriptor style;
	private String transform=null;
	
	public TextDescriptor() {
		// TODO Auto-generated constructor stub
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@XmlAttribute(name="font-weight")
	@JsonProperty("font-weight")
	public String getFont_weight() {
		return font_weight;
	}

	@JsonProperty("font-weight")
	public void setFont_weight(String font_weight) {
		this.font_weight = font_weight;
	}

	public StyleDescriptor getStyle() {
		return style;
	}

	public void setStyle(StyleDescriptor style) {
		this.style = style;
	}

	@XmlAttribute(name="font-size")
	@JsonProperty("font-size")
	public Integer getFont_size() {
		return font_size;
	}

	@JsonProperty("font-size")
	public void setFont_size(Integer font_size) {
		this.font_size = font_size;
	}

	public String getTransform() {
		return transform;
	}

	public void setTransform(String transform) {
		this.transform = transform;
	}

}
