package org.sius.workflow.view;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Kompletny model workflow
 * @author marcin
 *
 */
@XmlRootElement
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class TransportObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8353014296919729967L;

	private Graph graph;
	private Map<String, CellProperty> properties=new HashMap<String,CellProperty>();
	
	public TransportObject() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Zwraca wizualny wygląd diagramu
	 * @return Diagram
	 */
	@JsonProperty(value="layout")
	public Graph getGraph() {
		return graph;
	}

	/**
	 * Pobiera wizualny wygląd diagramu
	 * @param graph Diagram
	 */
	@JsonProperty(value="layout")
	public void setGraph(Graph graph) {
		this.graph = graph;
	}

	/**
	 * Zwraca właściwości procesu workflow
	 * @return Mapa właściwości
	 */
	@JsonProperty
	public Map<String, CellProperty> getProperties() {
		return properties;
	}

	/**
	 * Pobiera właściwości procesu workflow
	 * @param properties Mapa właściwości
	 */
	@JsonProperty
	public void setProperties(Map<String, CellProperty> properties) {
		this.properties = properties;
	}

}
