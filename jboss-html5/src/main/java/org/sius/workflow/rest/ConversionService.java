package org.sius.workflow.rest;

import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jdom.Document;
import org.sius.workflow.view.Cell;
import org.sius.workflow.view.Graph;
import org.sius.workflow.view.TransportObject;
import org.sius.workflow.view.Type;
import org.sius.workflow.xml.Create_XML;

/**
 * Jest to restowy webservice odpowiedzialny za konwersję modelu do formatu BPML i jego wczytywanie
 * @author marcin
 *
 */
@Path("/conversion")
@Stateless
public class ConversionService {
	
	/**
	 * Zapisuje model do formatu BPML
	 * @param to Jest to model obsługiwany przez interfejs przeglądarkowy
	 * @return XML jako TEXT_PLAIN
	 */
	@POST
	@Path("save")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String saveToXml(TransportObject to){
		return Create_XML.generateXmlString(to);
	}

	/**
	 * Wczytuje model z pliku BPML
	 * @param xml
	 * @return Model w formacie JSON
	 */
	@GET
	@Path("load")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public TransportObject loadFromXml(String xml){
		/*
		Resource asset = ResourceFactory.newByteArrayResource(xml.getBytes());
		Properties properties= new Properties();
		KieSession ksession = SessionFactory.getSession(asset, properties);
		SessionFactory.runAllProcesses(ksession);
		*/
		Graph graph = generateSampleGraph();
		TransportObject to = new TransportObject();
		to.setGraph(graph);
		return to;
	}

	private String generateFakeXml(Graph g) {
		StringBuilder xml = new StringBuilder("<?xmlns:\"This is a fake xml stub!\"?>\n<xml>\n");
		for (Cell cell : g.getCells()) {
			if ((cell != null) && (cell.recognize() != null)) {
				switch (cell.recognize()) {
				case START:
					xml.append("<start/>\n");
					break;
				case STOP:
					xml.append("<stop/>\n");
					break;
				case ARROW:
					xml.append("<arrow/>\n");
					break;
				case AUTOMATIC:
					xml.append("<automatic/>\n");
					break;
				case MANUAL:
					xml.append("<manual/>\n");
					break;
				default:
					break;
				}
			}
		}
		xml.append("</xml>");
		String xml_string = xml.toString();
		return xml_string;
	}

	/**
	 * Generuje przykładowy model widoku diagramu na potrzeby testów
	 * @return Model widoku diagramu
	 */
	public Graph generateSampleGraph() {
		Graph graph = new Graph();
		Cell start = Cell.StartStateFactory(100, 100);
		graph.addCell(start);
		Cell dec = Cell.DecisionFactory(200, 100);
		graph.addCell(dec);
		Cell auto = Cell.ActionFactory(200, 200, Type.AUTOMATIC);
		graph.addCell(auto);
		Cell man = Cell.ActionFactory(400, 200, Type.MANUAL);
		graph.addCell(man);
		Cell stop = Cell.StopStateFactory(200, 300);
		graph.addCell(stop);
		graph.addCell(Cell.ArrowFactory(start, dec));
		graph.addCell(Cell.ArrowFactory(dec, auto));
		graph.addCell(Cell.ArrowFactory(dec, man));
		graph.addCell(Cell.ArrowFactory(auto, stop));
		graph.addCell(Cell.ArrowFactory(man, stop));
		return graph;
	}
}
