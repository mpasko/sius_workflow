package org.sius.workflow.rest;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Webservice służący do przyjmowania kliknięcia od użytkownika
 * @author marcin
 *
 */
@Path("/activate")
@Stateless
public class ActivationService {

	/**
	 * Metoda GET kliknięcia
	 * @param guid procesu
	 * @return Tekstowa odpowiedź
	 */
	@GET
	@Path("guid/{guid}")
	@Produces(MediaType.TEXT_PLAIN)
	public String activate(@PathParam("guid") String guid){
		
		return "Tkank you for proceeding";
	}
}
