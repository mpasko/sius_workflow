package org.sius.workflow.xml;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.jdom.Namespace;
import org.sius.workflow.view.Cell;
import org.sius.workflow.view.TransportObject;
import org.sius.workflow.view.Type;
 
/**
 * Tworzy Plik BPML na podstawie modelu
 * Uzywa biblioteki JDOM
 * @author rafal
 *
 */
public class Create_XML  {
	public static void main(String[] args) {
 
	  try {
 
		Document doc = generateXmlDocument(null);
		
		
		//wyswietlanie
		XMLOutputter xmlOutput = new XMLOutputter();
 
		
		xmlOutput.setFormat(Format.getPrettyFormat());
		xmlOutput.output(doc, new FileWriter("C:\\Users\\Rafal\\Desktop\\file2.xml"));
 
		System.out.println("Zakonczono zapisywanie pliku");
	  } catch (IOException io) {
		System.out.println(io.getMessage());
	  }
	}

	/**
	 * Tworzy XML jako string
	 * @param to Model
	 * @return XML jako string
	 */
	public static String generateXmlString(TransportObject to){
		XMLOutputter xmlOutput = new XMLOutputter();
		return xmlOutput.outputString(generateXmlDocument(to));
	}
	
	/**
	 * Tworzy XML jako JDOM dokument
	 * @param to Model
	 * @return JDOM dokument
	 */
	public static Document generateXmlDocument(TransportObject to) {
		Namespace namespace = Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance" );
		Namespace namespace2 = Namespace.getNamespace("bpmn2", "http://www.omg.org/spec/BPMN/20100524/MODEL");
		Namespace namespace3 = Namespace.getNamespace("bpmndi", "http://www.omg.org/spec/BPMN/20100524/DI");
		Namespace namespace4 = Namespace.getNamespace("dc", "http://www.omg.org/spec/DD/20100524/DC");
		Namespace namespace5 = Namespace.getNamespace("di", "http://www.omg.org/spec/DD/20100524/DI");
		Namespace namespace6 = Namespace.getNamespace("g", "http://www.jboss.org/drools/flow/gpd");
		Namespace namespace7 = Namespace.getNamespace("tns", "http://www.jboss.org/drools");
		Namespace namespace8 = Namespace.getNamespace("http://www.jboss.org/drools");
		Namespace namespace9 = Namespace.getNamespace("schemaLocation", "http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd http://www.jboss.org/drools drools.xsd http://www.bpsim.org/schemas/1.0 bpsim.xsd");
		Namespace namespace10 = Namespace.getNamespace("id", "Definition");
		Namespace namespace11 = Namespace.getNamespace("expressionLanguage", "http://www.mvel.org/2.0");
		Namespace namespace12 = Namespace.getNamespace("targetNamespace","_");
		Namespace namespace13 = Namespace.getNamespace("typeLanguage", "http://www.java.com/javaTypes");
		
		Element definitions = new Element("definitions", namespace);
		Document doc = new Document(definitions);
		doc.setRootElement(definitions);
 
		definitions.addNamespaceDeclaration(namespace);
		definitions.addNamespaceDeclaration(namespace2);
		definitions.addNamespaceDeclaration(namespace3);
		definitions.addNamespaceDeclaration(namespace4);
		definitions.addNamespaceDeclaration(namespace5);
		definitions.addNamespaceDeclaration(namespace6);
		definitions.addNamespaceDeclaration(namespace7);
		definitions.addNamespaceDeclaration(namespace8);
		definitions.addNamespaceDeclaration(namespace9);
		definitions.addNamespaceDeclaration(namespace10);
		definitions.addNamespaceDeclaration(namespace11);
		definitions.addNamespaceDeclaration(namespace12);
		definitions.addNamespaceDeclaration(namespace13);
		
		
		
		
		
		
		/********************
		
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
		xmlns:bpmn2="http://www.omg.org/spec/BPMN/20100524/MODEL" 
		xmlns:bpmndi="http://www.omg.org/spec/BPMN/20100524/DI" 
		xmlns:dc="http://www.omg.org/spec/DD/20100524/DC" 
		xmlns:di="http://www.omg.org/spec/DD/20100524/DI" 
		xmlns:g="http://www.jboss.org/drools/flow/gpd" 
		xmlns:tns="http://www.jboss.org/drools" 
		xmlns="http://www.jboss.org/drools" 
		xsi:schemaLocation="http://www.omg.org/spec/BPMN/20100524/MODEL BPMN20.xsd http://www.jboss.org/drools drools.xsd http://www.bpsim.org/schemas/1.0 bpsim.xsd" 
		id="Definition" 
		expressionLanguage="http://www.mvel.org/2.0" 
		targetNamespace="" 
		typeLanguage="http://www.java.com/javaTypes">
		
		***********************/
		
		/***********************ItemDefinition*****************************/
		
		int item_no=to.getGraph().getCells().size(); // ilosc itemow
		Element[] itemDefinition_tab = new Element[4*item_no] ;
		
		
		Element itemDefinition = new Element("itemDefinition");
		itemDefinition.setAttribute(new Attribute("id", "_employeeItem"));
		itemDefinition.setAttribute(new Attribute("structureRef", "java.lang.String"));
		doc.getRootElement().addContent(itemDefinition);
 
		Element itemDefinition2 = new Element("itemDefinition");
		itemDefinition2.setAttribute(new Attribute("id", "_reasonItem"));
		itemDefinition2.setAttribute(new Attribute("structureRef", "java.lang.String"));
		doc.getRootElement().addContent(itemDefinition2);
		
		
		Element itemDefinition3 = new Element("itemDefinition");
		itemDefinition3.setAttribute(new Attribute("id", "_performanceItem"));
		itemDefinition3.setAttribute(new Attribute("structureRef", "java.lang.String"));
		doc.getRootElement().addContent(itemDefinition3);
		
		
		int z=2;
		for( int i = 1 ; i < item_no+1 ; ++i )
		{
			z=i+1;;
		itemDefinition_tab[i] = new Element("itemDefinition");
		itemDefinition_tab[i].setAttribute(new Attribute("id", "_"+z+"-employeeItem"));
		itemDefinition_tab[i].setAttribute(new Attribute("structureRef", "java.lang.String"));
		doc.getRootElement().addContent(itemDefinition_tab[i]);
 
		itemDefinition_tab[i+1] = new Element("itemDefinition");
		itemDefinition_tab[i+1].setAttribute(new Attribute("id", "_"+z+"-reasonItem"));
		itemDefinition_tab[i+1].setAttribute(new Attribute("structureRef", "java.lang.String"));
		doc.getRootElement().addContent(itemDefinition_tab[i+1]);
		
		
		itemDefinition_tab[i+2] = new Element("itemDefinition");
		itemDefinition_tab[i+2].setAttribute(new Attribute("id", "_"+z+"-performanceItem"));
		itemDefinition_tab[i+2].setAttribute(new Attribute("structureRef", "java.lang.String"));
		doc.getRootElement().addContent(itemDefinition_tab[i+2]);
		z++;
		
		}
		
		
		/***********************process  *************************/
		
		
		Element process = new Element("process");
		process.setAttribute(new Attribute("id", "com.sample.evaluation"));
		process.setAttribute(new Attribute("name", "Evaluation"));
		process.setAttribute(new Attribute("isExecutable", "true"));
		process.setAttribute(new Attribute("processType", "Private"));
		doc.getRootElement().addContent(process);
		
		
		/******************property *******************/
		Element property1 = new Element("property");
		property1.setAttribute(new Attribute("id", "employee"));
		property1.setAttribute(new Attribute("itemSubjectRef", "_employeeItem"));
 
		process.addContent(property1);
		
		Element property2 = new Element("property");
		property2.setAttribute(new Attribute("id", "reason"));
		property2.setAttribute(new Attribute("itemSubjectRef", "_reasonItem"));
 
		process.addContent(property2);
		
		Element property3 = new Element("property");
		property3.setAttribute(new Attribute("id", "performance"));
		property3.setAttribute(new Attribute("itemSubjectRef", "_performanceItem"));
 
		process.addContent(property3);
		
		/*******************start Event******************/
		
		
		Element startEvent = new Element("startEvent");
		startEvent.setAttribute(new Attribute("id", "_1"));
		startEvent.setAttribute(new Attribute("name", " "));
		startEvent.addContent(new Element("outgoing").setText("SequenceFlow_1"));
		process.addContent(startEvent);
		
		
		
		// nie bede mieszal tu wsadz case switch w zaleznosci czy to jest usertask czy parallel gateaway ja nie wiem jak go bedziesz pobieral wiec wstawiam ci wzor na sucho, mamy zmienna x ktora oznacza id danego elementu i jak jest element to dajemy x++, dzieki czemu np. po 7 elementach endEvent ma id _7i jeszcze nazwy
		
		int x=1;
		
		/******* user Task************/
		
		Element userTask = new Element("userTask");
		userTask.setAttribute(new Attribute("id", "_"+x));
		userTask.setAttribute(new Attribute("name", "wstaw nazwe"));
		//w ingoing i outgoing moga byc petle w zaleznosci od parametrow
		userTask.addContent(new Element("incoming").setText(" wstaw SequenceFlow_dany numer"));
		userTask.addContent(new Element("outgoing").setText(" wstaw SequenceFlow_dany numer"));
		
		/****************ioSpecification **************/
			Element ioSpecification = new Element("ioSpecification");
			ioSpecification.setAttribute(new Attribute("id", "InputOutputSpecification_"+(x-1))); //id-1 w naszym przypadku x:)
		
			
				Element dataInput = new Element("dataInput");
				dataInput.setAttribute(new Attribute("id", "_"+x+"_reasonInput"));
				dataInput.setAttribute(new Attribute("name","reason"));
				ioSpecification.addContent(dataInput);
			
				Element dataInput2 = new Element("dataInput");
				dataInput2.setAttribute(new Attribute("id", "_"+x+"_CommentInput"));
				dataInput2.setAttribute(new Attribute("name","Comment"));
				ioSpecification.addContent(dataInput2);
				
				Element dataInput3 = new Element("dataInput");
				dataInput3.setAttribute(new Attribute("id", "_"+x+"_SkippableInput"));
				dataInput3.setAttribute(new Attribute("name","Skippable"));
				ioSpecification.addContent(dataInput3);
				
				Element dataInput4 = new Element("dataInput");
				dataInput4.setAttribute(new Attribute("id", "_"+x+"_TaskNameInput"));
				dataInput4.setAttribute(new Attribute("name","TaskName"));
				ioSpecification.addContent(dataInput4);
				
				
				
				Element dataOutput = new Element("dataOutput");
				dataOutput.setAttribute(new Attribute("id", "_"+x+"_performanceOutput"));
				dataOutput.setAttribute(new Attribute("name","performance"));
				ioSpecification.addContent(dataOutput);
				
				
		
		
			userTask.addContent(ioSpecification);
		
		process.addContent(userTask);
		
		
	
		/******* parallel gateway*********/
		
		Element parallel_gateway = new Element("parallel_gateway");
		parallel_gateway.setAttribute(new Attribute("id", "_"+x));
		parallel_gateway.setAttribute(new Attribute("name", "wstaw nazwe"));
		parallel_gateway.setAttribute(new Attribute("gatewayDirection", "wstaw zmienna gateway_direction"));
		//w ingoing i outgoing moga byc petle w zaleznosci od parametrow
		parallel_gateway.addContent(new Element("incoming").setText(" wstaw SequenceFlow_dany numer"));
		parallel_gateway.addContent(new Element("outgoing").setText(" wstaw SequenceFlow_dany numer"));
		process.addContent(parallel_gateway);
		
		
		/******************* end Event************************/
		
		Element endEvent = new Element("endEvent");
		endEvent.setAttribute(new Attribute("id", "tutaj koncowy x"));
		endEvent.setAttribute(new Attribute("name", " "));
		endEvent.addContent(new Element("incoming").setText("SequenceFlow_dany numer"));
		process.addContent(endEvent);
		
		
		
		
		/**************** sequence Flow *****************************/
		
		Element sequenceFlow = new Element("sequenceFlow");
		sequenceFlow.setAttribute(new Attribute("id", "SequenceFlow_id"));
		sequenceFlow.setAttribute(new Attribute("name", " "));
		sequenceFlow.setAttribute(new Attribute("sourceRef", "wpisz dane skad idzie sequenceflow "));
		sequenceFlow.setAttribute(new Attribute("targetRef", "wpisz dane dokad idzie sequenceflow "));
		
		process.addContent(sequenceFlow);
		
		
		/****************** BPMNDiagram **********************/
		List<Cell> cells = to.getGraph().getCells();
		addBpmnDiagram(doc, cells);
		return doc;
	}

	/**
	 * Dodaje sekcję BpmnDiagram
	 * @param doc Dokument
	 * @param cells Elementy diagramu
	 */
	public static void addBpmnDiagram(Document doc, List<Cell> cells) {
		Element BPMNDiagram = new Element("BPMNDiagram");
		
		
		doc.getRootElement().addContent(BPMNDiagram);
		BPMNDiagram.setAttribute(new Attribute("id", "BPMNDiagram_1")); //raczej bedzie tylko jeden
			
			/********************BPMNPlane ****************/
			Element BPMNPlane = new Element("BPMNPlane");
			BPMNPlane.setAttribute(new Attribute("id", "BPMNPlane_Process_1"));  //tez tylko jeden:)
			BPMNPlane.setAttribute(new Attribute("bpmnElement", "com.sample.evaluation")); //mozna zmienic element
			
			Integer num = 1;
			for (Cell cell : cells) {	
				// tu liczymy prosto id od 1 w gore, caly czas robimy inkremnetacje o 1
				if (cell.recognize()==Type.ARROW){
					// tu liczymy prosto id od 1 w gore, caly czas robimy inkremnetacje o 1
					String id2 = "BPMNEdge_SequenceFlow_"+cell.getId();
					addBpmnEdge(BPMNPlane, id2, cell);
				} else {
					String id = "BPMNShape_"+cell.getId();
					addBpmnShape(BPMNPlane, num, id, cell);
					num++;
				}
				
			}
		
		
		BPMNDiagram.addContent(BPMNPlane);
	}

	/**
	 * Dodaje strałkę do diagramu
	 * @param BPMNPlane Plane na którym kładziemy element
	 * @param id2 Identyfikator elementu
	 * @param edge Strzałka
	 */
	public static void addBpmnEdge(Element BPMNPlane, String id2, Cell edge) {
		/****************** BPMN Edge*****************************/
		Element BPMNEdge = new Element("BPMNEdge");				//tutaj znow w petli BPMNShape
		BPMNEdge.setAttribute(new Attribute("id", id2));  
		BPMNEdge.setAttribute(new Attribute("bpmnElement", "piszemy laczacego sequenceFlowa"));
		BPMNEdge.setAttribute(new Attribute("sourceElement", edge.getSource().getId()));
		BPMNEdge.setAttribute(new Attribute("targetElement", edge.getTarget().getId()));
		/*
		 //U nas nie ma waypointow, wszystko idzie bezposrednio 
			Element waypoint = new Element("waypoint"); //okreslamy waypointy moze byc petla
			waypoint.setAttribute(new Attribute("type", "Point"));  
			waypoint.setAttribute(new Attribute("x", "0")); 
			waypoint.setAttribute(new Attribute("y", "0"));
			BPMNEdge.addContent(waypoint);
		*/
		BPMNPlane.addContent(BPMNEdge);
	}
	
	/**
	 * Dodaje bloczek do diagramu
	 * @param BPMNPlane Plane na którym kładziemy elementu
	 * @param num Kolejny numer
	 * @param id Identyfikator elementu
	 * @param cell Bloczek
	 */
	public static void addBpmnShape(Element BPMNPlane, Integer num, String id,
			Cell cell) {
		/*********************BPMNShape ***************/
		Element BPMNShape = new Element("BPMNShape");				//tutaj znow w petli BPMNShape
		BPMNShape.setAttribute(new Attribute("id", id));  
		BPMNShape.setAttribute(new Attribute("bpmnElement", num.toString()));

//dodajemy do kazdego BPMNShape wspolrzedne bounds
			Element bounds = new Element("bounds");
			bounds.setAttribute(new Attribute("height", cell.getSize().getHeight().toString()));  
			bounds.setAttribute(new Attribute("width", cell.getSize().getHeight().toString())); 
			bounds.setAttribute(new Attribute("x", cell.getPosition().getX().toString()));  
			bounds.setAttribute(new Attribute("y", cell.getPosition().getY().toString()));  

			BPMNShape.addContent(bounds);

		BPMNPlane.addContent(BPMNShape);
	}
}