package org.sius.workflow.simulation;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;

/**
 * Builder Routy Camela odpowiedzialnej za obsługę kolejki JMS
 * @author marcin
 *
 */
public class EmailRouteBuilder extends RouteBuilder{
	public class NotifyProcessor implements Processor{

		private String url;

		public NotifyProcessor(String email, String url) {
			// TODO Auto-generated constructor stub
			this.url = url;
		}

		@Override
		public void process(Exchange exchange) throws Exception {
			// TODO Auto-generated method stub
			Message in = exchange.getIn();
			StringBuilder sb = new StringBuilder("Dear User,\n\n");
	        sb.append("You receives this message, because there is a waiting business process "
	                + "that requires your immediate attention\n\n");
	        sb.append("Please follow link below to proceed:\n");
	        sb.append(url).append("\n\n");
	        sb.append("Best regards,\nBusiness process workflow tool");
			in.setBody(sb);
		}

	}
	private String email;
	private String url;
	private static int id;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void build(){
		
	}
	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		from("direct:sendNotification")
		.process(new NotifyProcessor(email,url))
		.log("Sending notification, kpi low").setHeader("subject", simple("Process notification"))
		.to("smtps://smtp.gmail.com:465?password=workflow123&username=workflow.builder.tool&To="+email+";");
		id++;
	}
}
