package org.sius.workflow.simulation;

import java.io.IOException;
import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.sius.workflow.view.Graph;

/**
 * Jest to specjalistyczny serwlet umożliwiający komunikację Reverse-AJAX z interfejsem przeglądarkowym
 * Stworzony na potrzeby wizualizacji symulacji uruchomienia workflow
 * @author marcin
 *
 */
@WebServlet(urlPatterns = "/simulation", asyncSupported = true)
public class SimulationServlet extends HttpServlet{

	private final Queue<AsyncContext> asyncContexts = new ConcurrentLinkedQueue<AsyncContext>();
	/**
	 * 
	 */
	private static final long serialVersionUID = -278204719824190540L;
	
	public Logger log = Logger.getLogger("SimulationServlet");

	public SimulationServlet() {
		// TODO Auto-generated constructor stub
	}
	
	private final Simulator sim=new Simulator();
	
	private final Thread generator = new Thread("Event generator") {
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    Thread.sleep(1000);
                    while (!asyncContexts.isEmpty()) {
                        AsyncContext asyncContext = asyncContexts.poll();
                        HttpServletResponse peer = (HttpServletResponse) asyncContext.getResponse();
                    	JSONArray jsonArray = new JSONArray();
                        for(String id :sim.getActiveTranslated()){
                        	jsonArray.put(id);
                        }
                        String message = jsonArray.toString();
                        peer.getWriter().write(message);
                        peer.setStatus(HttpServletResponse.SC_OK);
                        peer.setContentType("application/json");
                        asyncContext.complete();
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } catch (IOException e) {
                    throw new RuntimeException(e.getMessage(), e);
                }
                sim.nextStep();
            }
        }
    };
    
    /**
     * Start serwletu
     */
    @Override
    public void init() throws ServletException {
        generator.start();
    }

    /**
     * Zastopowanie serwletu
     */
    @Override
    public void destroy() {
        generator.interrupt();
    }

    /**
     * Przyjmuje request w celu zarejestrowania się klienta w kolejce rządań oczekujących
     * Przy każdym zdarzeniu po stronie serwera, rządanie jest pobierane i odsyłana jest odpowiedź do klienta
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AsyncContext asyncContext = req.startAsync();
        asyncContext.setTimeout(0);
        asyncContexts.offer(asyncContext);
    }
    
    /**
     * Przyjmuje request HTTP-POST
     * Jest to etap przesłania do symulatora całego modelu workflow
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	String data = extractPostRequestBody(req);
    	//log.info(data);
    	ObjectMapper mapper = new ObjectMapper();
    	Graph graph = mapper.readValue(data, Graph.class);
    	if (graph!= null && !graph.getCells().isEmpty()){
    		sim.updateGraph(graph);
    	}
    }
    
    static String extractPostRequestBody(HttpServletRequest request) throws IOException {
    	Scanner s = new Scanner(request.getInputStream(), "UTF-8");
    	String body = "";
        try{
        	if ("POST".equalsIgnoreCase(request.getMethod())) {
        		s.useDelimiter("\\A");
        		body = s.hasNext() ? s.next() : "";
        	}
        }
        finally{
        	s.close();
        }
        return body;
    }
}
