package org.sius.workflow.simulation;

import javax.xml.ws.Endpoint;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultCamelContext;

/**
 * Klasa odpowiedzialna za wysyłanie notyfikacji na kolejkę JMS na temat manualnej akcji oczekujacej
 * @author marcin
 *
 */
public class Sender{
	
	/**
	 * Wysłanie notyfikacji do użytkownika
	 * @param email Email użytkownika do wysłania
	 * @param url Url który użytkownik musi odwiedzić żeby wznowić proces
	 */
	public void sendNotification(String email, String url){
		CamelContext context = new DefaultCamelContext();
//		context.addEndpoint(uri, new DefaultE)
		EmailRouteBuilder builder = new EmailRouteBuilder();
		builder.setUrl(url);
		builder.setEmail(email);
		try {
			context.addRoutes(builder);
			context.start();
			ProducerTemplate template = context.createProducerTemplate();
			template.sendBody("direct:sendNotification","");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void removeSubscriber(String email){
		
	}
}
