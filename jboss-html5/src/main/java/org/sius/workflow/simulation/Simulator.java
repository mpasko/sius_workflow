package org.sius.workflow.simulation;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.sius.workflow.view.Cell;
import org.sius.workflow.view.Graph;
import org.sius.workflow.view.Type;

/**
 * Jest to główny zarządca symulacji procesu
 * @author marcin
 *
 */
public class Simulator{

	private Graph graph = new Graph();
	private List<String> active = new LinkedList<String>();
	
	public static Map<String,Simulator> processMap=new HashMap<String, Simulator>();
	
	public Simulator() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Następny krok symulacji
	 */
	public void nextStep(){
		List<String> newactive = new LinkedList<String>();
		for (String id : active){
			for (Cell cell : graph.getCells()){
				if (cell != null && cell.recognize()==Type.ARROW){
					if (cell.getSource().getId().equalsIgnoreCase(id)){
						newactive.add(cell.getTarget().getId());
					}
				}
				if (cell != null && cell.recognize()==Type.MANUAL){
					if(cell.getId().equalsIgnoreCase(id)){
						Sender send = new Sender();
						UUID guid = UUID.randomUUID();
						String url="http://localhost:8080/jboss-html5/rest/activate/guid/"+guid;
						processMap.put(guid.toString(), this);
						send.sendNotification("pasko@student.agh.edu.pl", url);
					}
				}
			}
		}
		active = newactive;
	}
	
	/**
	 * Zwraca listę ID-ków wszystkich aktywnych bloczków w danym momencie
	 * @return Lista ID
	 */
	public List<String> getActiveTranslated(){
		return active;
	}

	/**
	 * Przekazuje do symulatora aktualny model do symulacji
	 * @param graph2
	 */
	public void updateGraph(Graph graph2) {
		this.graph = graph2;
		this.active = new LinkedList<String>();
		for (Cell cell : graph.getCells()){
			if (cell.recognize()==Type.START){
				this.active.add(cell.getId());
			}
		}
	}
	
}
