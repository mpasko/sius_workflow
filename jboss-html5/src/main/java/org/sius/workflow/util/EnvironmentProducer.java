package org.sius.workflow.util;

import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;

/**
 * Provider podstawowych zależności w aplikacji J2EE
 * @author marcin
 *
 */
@ApplicationScoped
public class EnvironmentProducer {
	@PersistenceUnit(unitName = "org.jbpm.persistence.jpa")
    private EntityManagerFactory emf;


    @Produces
    public EntityManagerFactory getEntityManagerFactory() {
        return this.emf;
    }
    
    
    /*  *x/
    @Inject
    private UserGroupCallback myCallback;
    
    
    /*  *X/
    @Produces
    public UserGroupCallback getUserGroupCallback() {
    	return new MyUserGroupCallback();
    }
    /*  */


    @Produces
    @PersistenceContext
    @RequestScoped
    public EntityManager getEntityManager() {
        EntityManager em = emf.createEntityManager();
        return em;
    }


    public void close(@Disposes EntityManager em) {
        em.close();
    }

/*
    @Produces
    public IdentityProvider produceIdentityProvider() {
        return new IdentityProvider() {

			@Override
			public String getName() {
				return "org.sius.workflow.IdentityProvider";
			}

			@Override
			public List<String> getRoles() {
				return new LinkedList<String>();
			}
        };
    }
*/
    @Produces
    public Logger produceLog(InjectionPoint injectionPoint) {
        return Logger.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
    }
}
